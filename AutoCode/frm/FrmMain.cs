﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutoCode
{
    public partial class FrmMain : Form
    {
        DataTable _TableConfig = new DataTable();
        string _FileConfig = Application.StartupPath + "\\Config.csv";
        string _strDatabase = "";
        string _strTableName = "";

        public FrmMain()
        {
            InitializeComponent();

            InitLVColumnbase();
            InitListViewTable();
            InitListViewColumn();

            Utils.DrawLVStyle(ref LVDatabase);
            Utils.SizeLastColumn_LV(LVDatabase);
            Utils.DrawLVStyle(ref LVColumn);
            Utils.SizeLastColumn_LV(LVColumn);
            Utils.DrawLVStyle(ref LVTable);
            Utils.SizeLastColumn_LV(LVTable);

            txtTitle.MouseDown += Frm_Main_MouseDown;
            txtTitle.MouseMove += Frm_Main_MouseMove;
            txtTitle.MouseUp += Frm_Main_MouseUp;
            btn_AutoString.Click += Btn_AutoString_Click; ;
            btn_Clear.Click += Btn_Clear_Click;
            btnClose.Click += btnClose_Click;
            btnOpenFolder.Click += BtnOpenFolder_Click;
            btnAutoCode.Click += BtnAutoCode_Click;

            LVDatabase.ItemActivate += LVDatabase_ItemActivate;
            LVTable.ItemActivate += LVTable_ItemActivate;
            cbo_Server.SelectedIndexChanged += Cbo_Server_SelectedIndexChanged;
            txt_Path.Text = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            if (File.Exists(_FileConfig))
            {
                _TableConfig = ReadCSV(_FileConfig);

                foreach (DataRow r in _TableConfig.Rows)
                {
                    cbo_Server.Items.Add(r[0].ToString());
                    cbo_UserName.Items.Add(r[1].ToString());
                    cbo_Password.Items.Add(r[2].ToString());
                }
            }
        }

        private void Btn_AutoString_Click(object sender, EventArgs e)
        {
            AutoStringBuilder();
        }

        private void Btn_Clear_Click(object sender, EventArgs e)
        {
            txt_After.Clear();
        }

        private void InitLVColumnbase()
        {
            ListView LV = LVDatabase;

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên dữ liệu";

            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }

        private void InitListViewTable()
        {
            ListView LV = LVTable;

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên bảng";

            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }

        private void InitListViewColumn()
        {
            ListView LV = LVColumn;

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên cột";
            colHead.Name = "COLUMN_NAME";

            colHead.Width = 160;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Kiểu dữ liệu";
            colHead.Name = "DATA_TYPE";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Độ dài";
            colHead.Name = "CHAR_LENGTH";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Cột số";
            colHead.Name = "NUMERIC";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Cột NULL";
            colHead.Name = "IS_NULL";
            colHead.Width = 70;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            LVDatabase.Items.Clear();

            this.Cursor = Cursors.WaitCursor;
            DataTable zTable = Access_Data.GetDatabase(cbo_Server.Text, cbo_UserName.Text, cbo_Password.Text);

            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVDatabase;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            int i = 0;
            foreach (DataRow r in zTable.Rows)
            {
                DataRow nRow = zTable.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow[0].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

                i++;
            }
            this.Cursor = Cursors.Default;

            DataRow[] Row = _TableConfig.Select("Server = '" + cbo_Server.Text + "'");
            if (Row.Length == 0)
            {
                DataRow rNew = _TableConfig.NewRow();

                rNew[0] = cbo_Server.Text;
                rNew[1] = cbo_UserName.Text;
                rNew[2] = cbo_Password.Text;

                _TableConfig.Rows.Add(rNew);

                WriteCSV(_TableConfig, _FileConfig);
            }
        }

        private void BtnOpenFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            txt_Path.Text = folderBrowserDialog1.SelectedPath;
        }

        private void BtnAutoCode_Click(object sender, EventArgs e)
        {
            DirectoryInfo nDir = new DirectoryInfo(txt_Path.Text);
            if (!nDir.Exists)
            {
                nDir.Create();
            }

            string zFileData = txt_Path.Text + "\\" + txt_DataClass.Text;
            if (chkData.Checked)
            {
                if (File.Exists(zFileData))
                {
                    File.Delete(zFileData);
                }

                CreateFileData(zFileData);
            }

            string zFileModal = txt_Path.Text + "\\" + txt_ModelClass.Text;
            string zFileInfo = txt_Path.Text + "\\" + txt_InfoClass.Text;
            if (chkModel.Checked)
            {
                if (File.Exists(zFileModal))
                {
                    File.Delete(zFileModal);
                }

                CreateModel(zFileModal);


                if (chkInfo.Checked)
                {
                    if (File.Exists(zFileInfo))
                    {
                        File.Delete(zFileInfo);
                    }

                    CreateFileInfo(zFileInfo);
                }
            }
            else
            {
                if (File.Exists(zFileInfo))
                {
                    File.Delete(zFileInfo);
                }

                CreateFileInfoV1(zFileInfo);
            }


            Process.Start(txt_Path.Text);
        }

        private void LVDatabase_ItemActivate(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            _strDatabase = LVDatabase.SelectedItems[0].SubItems[1].Text;
            Access_Data._ConnectString = "Data Source = " + cbo_Server.Text + ";DataBase= " + _strDatabase + ";User=" + cbo_UserName.Text + ";Password= " + cbo_Password.Text;
            DataTable zTable = Access_Data.GetListTable();

            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVTable;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int i = 0;
            foreach (DataRow r in zTable.Rows)
            {
                DataRow nRow = zTable.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TABLE_NAME"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

                i++;
            }
            this.Cursor = Cursors.Default;
        }

        private void LVTable_ItemActivate(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            _strTableName = LVTable.SelectedItems[0].SubItems[1].Text;
            DataTable zTable = Access_Data.StructOfTable(_strTableName);

            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVColumn;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = zTable.Rows.Count;

            for (int i = 0; i < n; i++)
            {
                DataRow nRow = zTable.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["COLUMN_NAME"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["DATA_TYPE"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CHARACTER_MAXIMUM_LENGTH"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["NUMERIC_PRECISION"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["IS_NULLABLE"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

            txt_DataClass.Text = _strTableName.Remove(0, 4) + "_Data.cs";
            txt_InfoClass.Text = _strTableName.Remove(0, 4) + "_Info.cs";
            txt_ModelClass.Text = _strTableName.Remove(0, 4) + "_Model.cs";
        }

        private void Cbo_Server_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_Server.Text.Trim().Length > 0)
            {
                DataRow[] Row = _TableConfig.Select("Server = '" + cbo_Server.Text + "'");
                if (Row.Length > 0)
                {
                    cbo_UserName.Text = Row[0][1].ToString();
                    cbo_Password.Text = Row[0][2].ToString();
                }
            }
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        //----------
        #region [--Data--]
        private void CreateFileData(string In_FileName)
        {
            FileStream fs = new FileStream(In_FileName, FileMode.Create);
            StreamWriter w = new StreamWriter(fs, Encoding.UTF8);
            w.WriteLine("using System.Configuration;");
            w.WriteLine("using System;");
            w.WriteLine("using System.Collections.Generic;");
            w.WriteLine("using System.Text;");
            w.WriteLine("using System.Data;");
            w.WriteLine("using System.Data.SqlClient;");
            if (txt_Using.Text.Trim().Length > 0)
            {
                w.WriteLine("using " + txt_Using.Text.Trim() + ";");
            }

            w.WriteLine("namespace " + txt_NameSpace.Text);
            w.WriteLine("{");
            w.WriteLine("public partial class " + _strTableName.Remove(0, 4) + "_Data");
            w.WriteLine("{");

            //w.WriteLine("public static DataTable List(string PartnerNumber, out string Message)");
            //w.WriteLine("{");
            //w.WriteLine("DataTable zTable = new DataTable();");
            //w.WriteLine("string zSQL = \"SELECT  * FROM " + _strTableName + " WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber \" ;");
            //w.WriteLine("string zConnectionString =" + cbo_Connect.Text + ";");
            //w.WriteLine("try");
            //w.WriteLine("{");
            //w.WriteLine("SqlConnection zConnect = new SqlConnection(zConnectionString);");
            //w.WriteLine("zConnect.Open();");
            //w.WriteLine("SqlCommand zCommand = new SqlCommand(zSQL, zConnect);");
            //w.WriteLine("zCommand.Parameters.Add(\"@PartnerNumber\", SqlDbType.NVarChar).Value = PartnerNumber;");
            //w.WriteLine("SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);");

            //w.WriteLine("zAdapter.Fill(zTable);");
            ////---- Close Connect SQL ----
            //w.WriteLine("zCommand.Dispose();");
            //w.WriteLine("zConnect.Close();");
            //w.WriteLine("}");
            //w.WriteLine("catch (Exception ex)");
            //w.WriteLine("{");
            //w.WriteLine("Message= ex.ToString();");
            //w.WriteLine("}");

            //w.WriteLine("return zTable;");
            //w.WriteLine("}");
            //--------------
            string _strNameInfo = _strTableName.Remove(0, 4) + "_Info";
            w.WriteLine("public static List<" + _strNameInfo + "> List(out string Message)");
            w.WriteLine("{");
            w.WriteLine("var zTable = new DataTable();");
            w.WriteLine("string zSQL = \"SELECT * FROM " + _strTableName + " WHERE RecordStatus != 99\";");
            w.WriteLine("string zConnectionString =" + cbo_Connect.Text + ";");
            w.WriteLine("try");
            w.WriteLine("{");
            w.WriteLine("SqlConnection zConnect = new SqlConnection(zConnectionString);");
            w.WriteLine("zConnect.Open();");
            w.WriteLine("SqlCommand zCommand = new SqlCommand(zSQL, zConnect);");
            w.WriteLine("SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);");

            w.WriteLine("zAdapter.Fill(zTable);");
            //---- Close Connect SQL ----
            w.WriteLine("zCommand.Dispose();");
            w.WriteLine("zConnect.Close(); Message = string.Empty;");
            w.WriteLine("}");
            w.WriteLine("catch (Exception ex)");
            w.WriteLine("{");
            w.WriteLine("Message = ex.ToString();");
            w.WriteLine("}");

            string txt = "var zList = new List<" + _strNameInfo + ">();";
            txt += Environment.NewLine;
            txt += "foreach (DataRow r in zTable.Rows)";
            txt += "{";
            txt += "zList.Add(new " + _strNameInfo + "() {";

            int n = LVColumn.Items.Count;
            for (int i = 0; i < n; i++)
            {
                string zFieldName = LVColumn.Items[i].SubItems[1].Text;
                string zType = LVColumn.Items[i].SubItems[2].Text;
                string zPrimary = LVColumn.Items[i].SubItems[5].Text;
                switch (zType)
                {

                    case "uniqueidentifier":
                    case "char":
                    case "nchar":
                    case "ntext":
                    case "nvarchar":
                        //txt += zFieldName + "= r[\"" + zFieldName + "\"].ToString(),";
                        txt += zFieldName + "=(r[\"" + zFieldName + "\"] == DBNull.Value ? \"\" : r[\"" + zFieldName + "\"].ToString()),";
                        break;
                    case "int":
                        //txt += zFieldName + "= r[\"" + zFieldName + "\"].ToInt(),";
                        txt += zFieldName + "=(r[\"" + zFieldName + "\"] == DBNull.Value ? 0 : int.Parse(r[\"" + zFieldName + "\"].ToString())),";
                        break;
                    case "date":
                    case "datetime":
                        txt += zFieldName + "=(r[\"" + zFieldName + "\"] == DBNull.Value ? DateTime.MinValue : (DateTime)r[\"" + zFieldName + "\"]),";
                        break;

                    case "money":
                    case "numeric":
                        //txt += zFieldName + "= r[\"" + zFieldName + "\"].ToDouble(),";
                        txt += zFieldName + "=(r[\"" + zFieldName + "\"] == DBNull.Value ? 0 : double.Parse(r[\"" + zFieldName + "\"].ToString())),";
                        break;

                    case "bit":
                        //txt += zFieldName + "= r[\"" + zFieldName + "\"].ToBool(),";
                        txt += zFieldName + "=(r[\"" + zFieldName + "\"] == DBNull.Value ? false : bool.Parse(r[\"" + zFieldName + "\"].ToString())),";
                        break;

                    case "bigint":
                        //txt += zFieldName + "= r[\"" + zFieldName + "\"].ToLong(),";
                        txt += zFieldName + "=(r[\"" + zFieldName + "\"] == DBNull.Value ? 0 : long.Parse(r[\"" + zFieldName + "\"].ToString())),";
                        break;

                    case "float":
                        //txt += zFieldName + "= r[\"" + zFieldName + "\"].ToFloat(),";
                        txt += zFieldName + "=(r[\"" + zFieldName + "\"] == DBNull.Value ? 0 : float.Parse(r[\"" + zFieldName + "\"].ToString())),";
                        break;

                    default:
                        txt += zFieldName + "= r[\"" + zFieldName + "\"].ToString(),";
                        break;
                }
                txt += Environment.NewLine;
            }

            txt += "});";
            txt += "}";

            w.WriteLine(txt);
            w.WriteLine("return zList;");


            w.WriteLine("}");
            w.WriteLine("}");

            w.WriteLine("}");

            // Bảo đảm tất cả dữ liệu được ghi từ buffer.
            w.Flush();
            // Đóng file.
            w.Close();
            fs.Close();
        }
        #endregion

        #region [--Model--]
        private void CreateModel(string In_FileName)
        {
            string _strNameInfo = _strTableName.Remove(0, 4) + "_Model";

            FileStream fs = new FileStream(In_FileName, FileMode.Create);
            StreamWriter w = new StreamWriter(fs, Encoding.UTF8);

            w.WriteLine("using System.Configuration;");
            w.WriteLine("using System;");
            w.WriteLine("using System.Collections.Generic;");
            w.WriteLine("using System.Text;");
            w.WriteLine("using System.Data;");
            w.WriteLine("using System.Data.SqlClient;");
            if (txt_Using.Text.Trim().Length > 0)
            {
                w.WriteLine("using " + txt_Using.Text.Trim() + ";");
            }

            w.WriteLine("namespace " + txt_NameSpace.Text);
            w.WriteLine("{");
            w.WriteLine("public partial class " + _strTableName.Remove(0, 4) + "_Model");
            w.WriteLine("{");

            #region [ Field Name ]

            w.WriteLine("#region [ Field Name ]");
            //int i = 0;
            int n = LVColumn.Items.Count;
            string zKeyName = "";
            string zFieldName = "";
            string zType = "";
            for (int i = 0; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                zType = LVColumn.Items[i].SubItems[2].Text;

                switch (zType)
                {
                    case "char":
                        w.WriteLine("private string _" + zFieldName + " = \"\";");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                        }
                        break;

                    case "nchar":
                        w.WriteLine("private string _" + zFieldName + " = \"\";");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                        }
                        break;
                    case "nvarchar":
                        w.WriteLine("private string _" + zFieldName + " = \"\";");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                        }
                        break;
                    case "ntext":
                        w.WriteLine("private string _" + zFieldName + " = \"\";");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                        }
                        break;
                    case "int":
                        w.WriteLine("private int _" + zFieldName + " = 0;");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                        }
                        break;
                    case "date":
                        w.WriteLine("private DateTime _" + zFieldName + " = DateTime.MinValue;");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                        }
                        break;
                    case "datetime":
                        w.WriteLine("private DateTime _" + zFieldName + " = DateTime.MinValue;");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                        }
                        break;
                    case "time":
                        w.WriteLine("private TimeSpan _" + zFieldName + " = new TimeSpan();");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                        }
                        break;
                    case "money":
                        w.WriteLine("private double _" + zFieldName + " = 0;");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                        }
                        break;
                    case "bit":
                        w.WriteLine("private bool _" + zFieldName + ";");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                        }
                        break;
                    case "image":
                        w.WriteLine("private Image _" + zFieldName + ";");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                        }
                        break;

                    case "float":
                        w.WriteLine("private float _" + zFieldName + "= 0;");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                        }
                        break;
                    case "numeric":
                        w.WriteLine("private float _" + zFieldName + "= 0;");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                        }
                        break;
                    case "uniqueidentifier":
                        w.WriteLine("private string _" + zFieldName + " = \"\";");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                        }
                        break;

                    case "bigint":
                        w.WriteLine("private long _" + zFieldName + "= 0;");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                        }
                        break;
                }


            }


            w.WriteLine("#endregion");
            #endregion

            #region [ Properties ]

            w.WriteLine("#region [ Properties ]");
            // BEGIN Properties
            for (int i = 0; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                zType = LVColumn.Items[i].SubItems[2].Text;

                switch (zType)
                {
                    case "char":
                        w.WriteLine("public string " + zFieldName);
                        break;
                    case "nchar":
                        w.WriteLine("public string " + zFieldName);
                        break;
                    case "nvarchar":
                        w.WriteLine("public string " + zFieldName);
                        break;
                    case "ntext":
                        w.WriteLine("public string " + zFieldName);
                        break;
                    case "int":
                        w.WriteLine("public int " + zFieldName);
                        break;
                    case "date":
                        w.WriteLine("public DateTime " + zFieldName);
                        break;
                    case "datetime":
                        w.WriteLine("public DateTime " + zFieldName);
                        break;
                    case "time":
                        w.WriteLine("public TimeSpan " + zFieldName);
                        break;
                    case "money":
                        w.WriteLine("public double " + zFieldName);
                        break;
                    case "bit":
                        w.WriteLine("public bool " + zFieldName);
                        break;
                    case "image":
                        w.WriteLine("public Image " + zFieldName);
                        break;
                    case "float":
                        w.WriteLine("public float " + zFieldName);
                        break;
                    case "numeric":
                        w.WriteLine("public float " + zFieldName);
                        break;
                    case "uniqueidentifier":
                        w.WriteLine("public string " + zFieldName);
                        break;
                    case "bigint":
                        w.WriteLine("public long " + zFieldName);
                        break;
                }
                w.WriteLine("{");
                w.WriteLine("get { return _" + zFieldName + "; }");
                w.WriteLine("set { _" + zFieldName + " = value; }");
                w.WriteLine("}");

            }


            w.WriteLine("#endregion");
            #endregion

            w.WriteLine("}");
            w.WriteLine("}");

            // Bảo đảm tất cả dữ liệu được ghi từ buffer.
            w.Flush();
            // Đóng file.
            w.Close();
            fs.Close();
        }
        #endregion

        #region [--Info--]
        private void CreateFileInfo(string In_FileName)
        {
            string _strClassName = _strTableName.Remove(0, 4);

            int n = LVColumn.Items.Count;
            string zKeyName = "";
            string zKeyTypeSQL = "";
            string zKeyTypeCShap = "";
            string zFieldName = "";
            string zType = "";
            string strField = "";
            string zPrimary = "";

            #region Data type
            for (int i = 0; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                zType = LVColumn.Items[i].SubItems[2].Text;
                zPrimary = LVColumn.Items[i].SubItems[5].Text;
                switch (zType)
                {
                    case "uniqueidentifier":
                        if (i == 0 && zPrimary == "NO")
                        {
                            zKeyTypeSQL = "SqlDbType.UniqueIdentifier";
                            zKeyTypeCShap = "string";
                            zKeyName = zFieldName;
                        }
                        break;
                    case "char":
                        if (i == 0)
                        {
                            zKeyTypeSQL = "SqlDbType.Char";
                            zKeyTypeCShap = "string";
                            zKeyName = zFieldName;
                        }
                        break;

                    case "nchar":
                        if (i == 0)
                        {
                            zKeyTypeSQL = "SqlDbType.NChar";
                            zKeyTypeCShap = "string";
                            zKeyName = zFieldName;
                        }
                        break;
                    case "nvarchar":
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.NVarChar";
                            zKeyTypeCShap = "string";
                        }
                        break;
                    case "ntext":
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.NText";
                            zKeyTypeCShap = "string";
                        }
                        break;
                    case "int":
                        if (i == 0 && zPrimary == "NO")
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Int";
                            zKeyTypeCShap = "int";
                        }
                        break;
                    case "date":
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Date";
                            zKeyTypeCShap = "DateTime";
                        }
                        break;
                    case "datetime":
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.DateTime";
                            zKeyTypeCShap = "DateTime";
                        }
                        break;
                    case "time":
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Time";
                            zKeyTypeCShap = "TimeSpan";
                        }
                        break;
                    case "money":
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Money";
                            zKeyTypeCShap = "double";
                        }
                        break;
                    case "bit":
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Bit";
                            zKeyTypeCShap = "bool";
                        }
                        break;
                    case "image":
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Image";
                            zKeyTypeCShap = "Image";
                        }
                        break;

                    case "float":
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Float";
                            zKeyTypeCShap = "float";
                        }
                        break;
                    case "numeric":
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Float";
                            zKeyTypeCShap = "float";
                        }
                        break;
                    case "bigint":
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.BigInt";
                            zKeyTypeCShap = "long";
                        }
                        break;
                }
            }
            #endregion


            FileStream fs = new FileStream(In_FileName, FileMode.Create);
            StreamWriter w = new StreamWriter(fs, Encoding.UTF8);

            w.WriteLine("using System.Configuration;");
            w.WriteLine("using System;");
            w.WriteLine("using System.Collections.Generic;");
            w.WriteLine("using System.Text;");
            w.WriteLine("using System.Data;");
            w.WriteLine("using System.Data.SqlClient;");
            if (txt_Using.Text.Trim().Length > 0)
            {
                w.WriteLine("using " + txt_Using.Text.Trim() + ";");
            }
            w.WriteLine("namespace " + txt_NameSpace.Text);
            w.WriteLine("{");


            w.WriteLine("public partial class " + _strClassName + "_Info");
            w.WriteLine("{");

            w.WriteLine("public  " + _strClassName + "_Model " + _strClassName + " = new " + _strClassName + "_Model();");
            w.WriteLine("private string _Message = \"\";");
            w.WriteLine("public string Code ");
            w.WriteLine("{");
            w.WriteLine("get { ");
            w.WriteLine("if (_Message.Length >= 3)");
            w.WriteLine("return _Message.Substring(0, 3);");
            w.WriteLine("else return \"\";");
            w.WriteLine("}");
            w.WriteLine("}");
            w.WriteLine("public string Message ");
            w.WriteLine("{");
            w.WriteLine("get { return _Message; }");
            w.WriteLine("set { _Message = value; }");
            w.WriteLine("}");
            _strClassName += ".";

            #region [ Constructor Get Information ]

            w.WriteLine("#region [ Constructor Get Information ]");
            //Begin Class Info
            w.WriteLine("public " + _strClassName.Replace(".", "") + "_Info()");
            w.WriteLine("{");
            if (zKeyTypeSQL == "SqlDbType.UniqueIdentifier")
            {
                w.WriteLine(_strClassName + zKeyName + " = Guid.NewGuid().ToString();");
            }
            w.WriteLine("}");

            w.WriteLine("public " + _strClassName.Replace(".", "") + "_Info(" + zKeyTypeCShap + " " + zKeyName + ")");
            w.WriteLine("{");
            w.WriteLine("string zSQL = \"SELECT * FROM " + _strTableName + " WHERE " + zKeyName + " = @" + zKeyName + " AND RecordStatus != 99 \"; ");

            w.WriteLine("string zConnectionString = " + cbo_Connect.Text + ";");
            w.WriteLine("SqlConnection zConnect = new SqlConnection(zConnectionString);");
            w.WriteLine("zConnect.Open();");
            w.WriteLine("try");
            w.WriteLine("{");
            w.WriteLine("SqlCommand zCommand = new SqlCommand(zSQL, zConnect);");
            w.WriteLine("zCommand.CommandType = CommandType.Text;");
            if (zKeyTypeSQL == "SqlDbType.UniqueIdentifier")
            {
                w.WriteLine("zCommand.Parameters.Add(\"@" + zKeyName + "\", " + zKeyTypeSQL + ").Value = Guid.Parse(" + zKeyName + ");");
            }
            else
            {
                w.WriteLine("zCommand.Parameters.Add(\"@" + zKeyName + "\", " + zKeyTypeSQL + ").Value = " + zKeyName + ";");
            }

            w.WriteLine("SqlDataReader zReader = zCommand.ExecuteReader();");
            w.WriteLine("if (zReader.HasRows)");
            w.WriteLine("{");
            w.WriteLine("zReader.Read();");
            StringBuilder strCommand = new StringBuilder();
            StringBuilder strCommandCreated_Server = new StringBuilder();
            StringBuilder strCommandCreated_Client = new StringBuilder();
            StringBuilder strCommandUpdate = new StringBuilder();

            for (int i = 0; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                zType = LVColumn.Items[i].SubItems[2].Text;

                switch (zType)
                {
                    case "char":
                        strCommand.Append(_strClassName + zFieldName + " = zReader[\"" + zFieldName + "\"].ToString();").AppendLine();
                        break;
                    case "nchar":
                        strCommand.Append(_strClassName + zFieldName + " = zReader[\"" + zFieldName + "\"].ToString();").AppendLine();
                        break;
                    case "nvarchar":
                        strCommand.Append(_strClassName + zFieldName + " = zReader[\"" + zFieldName + "\"].ToString();").AppendLine();
                        break;
                    case "ntext":
                        strCommand.Append(_strClassName + zFieldName + " = zReader[\"" + zFieldName + "\"].ToString();").AppendLine();
                        break;
                    case "int":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append(_strClassName + zFieldName + " = int.Parse(zReader[\"" + zFieldName + "\"].ToString());").AppendLine();
                        break;
                    case "date":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append(_strClassName + zFieldName + " = (DateTime)zReader[\"" + zFieldName + "\"];").AppendLine();
                        break;
                    case "datetime":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append(_strClassName + zFieldName + " = (DateTime)zReader[\"" + zFieldName + "\"];").AppendLine();
                        break;
                    case "time":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append(_strClassName + zFieldName + " = (TimeSpan)zReader[\"" + zFieldName + "\"];").AppendLine();
                        break;
                    case "money":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append(_strClassName + zFieldName + " = double.Parse(zReader[\"" + zFieldName + "\"].ToString());").AppendLine();
                        break;
                    case "bit":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append(_strClassName + zFieldName + " = (bool)zReader[\"" + zFieldName + "\"];").AppendLine();
                        break;
                    case "image":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append(_strClassName + zFieldName + " = (Image)zReader[\"" + zFieldName + "\"];").AppendLine();
                        break;

                    case "float":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append(_strClassName + zFieldName + " = float.Parse(zReader[\"" + zFieldName + "\"].ToString());").AppendLine();
                        break;
                    case "numeric":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append(_strClassName + zFieldName + " = float.Parse(zReader[\"" + zFieldName + "\"].ToString());").AppendLine();
                        break;
                    case "uniqueidentifier":
                        strCommand.Append(_strClassName + zFieldName + " = zReader[\"" + zFieldName + "\"].ToString();").AppendLine();
                        //strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        //strCommand.Append(_strClassName + zFieldName + " = Guid.Parse(zReader[\"" + zFieldName + "\"].ToString());").AppendLine();
                        break;

                    case "bigint":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append(_strClassName + zFieldName + " = long.Parse(zReader[\"" + zFieldName + "\"].ToString());").AppendLine();
                        break;
                }
            }
            strCommand.AppendLine("_Message = \"200 OK\";");
            strCommand.AppendLine("}");
            strCommand.AppendLine("else");
            strCommand.AppendLine("{");
            strCommand.AppendLine("_Message = \"404 Not Found\";");
            strCommand.AppendLine("}");
            strCommand.AppendLine("zReader.Close();");
            strCommand.AppendLine("zCommand.Dispose();");
            strCommand.AppendLine("}");
            strCommand.AppendLine("catch (Exception Err)");
            strCommand.AppendLine("{");
            strCommand.AppendLine("_Message = \"500 \" + Err.ToString();");
            strCommand.AppendLine("}");
            strCommand.AppendLine("finally");
            strCommand.AppendLine("{");
            strCommand.AppendLine("zConnect.Close();");
            strCommand.AppendLine("}");
            strCommand.AppendLine("}");

            w.WriteLine(strCommand.ToString());
            w.WriteLine("#endregion");
            #endregion



            #region [ Constructor Create - Update Information ]

            w.WriteLine("#region [ Constructor Update Information ]");

            strCommand = new StringBuilder();
            strCommandCreated_Server = new StringBuilder();
            strCommandCreated_Client = new StringBuilder();
            strCommandUpdate = new StringBuilder();

            #region [ Create Key On Server ]
            strCommandCreated_Server.AppendLine("public string Create_KeyAuto()");
            strCommandCreated_Server.AppendLine("{");

            strCommandCreated_Server.AppendLine("//---------- String SQL Access Database ---------------");
            strCommandCreated_Server.AppendLine("    string zSQL = \"INSERT INTO " + _strTableName + " (\" ");
            strField = " + \" ";
            for (int i = 1; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                zType = LVColumn.Items[i].SubItems[2].Text;
                if (zFieldName != "CreatedOn" && zFieldName != "ModifiedOn")
                {
                    strField += zFieldName + " ";
                    if (i < n - 1)
                    {
                        strField += ", ";
                    }
                    else
                    {
                        strField += ") \"";
                    }
                }
            }
            strCommandCreated_Server.AppendLine(strField);
            strCommandCreated_Server.AppendLine(" + \" VALUES ( \"");

            strField = " + \" ";
            for (int i = 1; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                if (zFieldName != "CreatedOn" && zFieldName != "ModifiedOn")
                {
                    strField += "@" + zFieldName + " ";

                    if (i < n - 1)
                    {
                        strField += ", ";
                    }
                    else
                    {
                        strField += ") \";";
                    }
                }
            }
            strCommandCreated_Server.AppendLine(strField);
            #endregion

            #region [ Create Key at Client]
            strCommandCreated_Client.AppendLine("public string Create_KeyManual()");
            strCommandCreated_Client.AppendLine("{");

            strCommandCreated_Client.AppendLine("//---------- String SQL Access Database ---------------");
            strCommandCreated_Client.AppendLine("    string zSQL = \"INSERT INTO " + _strTableName + "(\" ");
            strField = " + \" ";
            for (int i = 0; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                zType = LVColumn.Items[i].SubItems[2].Text;
                if (zFieldName != "CreatedOn" && zFieldName != "ModifiedOn")
                {
                    strField += zFieldName + " ";
                    if (i < n - 1)
                    {
                        strField += ", ";
                    }
                    else
                    {
                        strField += ") \"";
                    }
                }
            }
            strCommandCreated_Client.AppendLine(strField);
            strCommandCreated_Client.AppendLine(" + \" VALUES ( \"");

            strField = " + \" ";
            for (int i = 0; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                if (zFieldName != "CreatedOn" && zFieldName != "ModifiedOn")
                {
                    strField += "@" + zFieldName + " ";

                    if (i < n - 1)
                    {
                        strField += ", ";
                    }
                    else
                    {
                        strField += ") \";";
                    }
                }
            }
            strCommandCreated_Client.AppendLine(strField);
            #endregion

            strCommand.AppendLine("string zResult = \"\"; ");
            strCommand.AppendLine("string zConnectionString = " + cbo_Connect.Text + ";");
            strCommand.AppendLine("SqlConnection zConnect = new SqlConnection(zConnectionString);");
            strCommand.AppendLine("zConnect.Open();");
            strCommand.AppendLine("try");
            strCommand.AppendLine("{");
            strCommand.AppendLine("SqlCommand zCommand = new SqlCommand(zSQL, zConnect);");
            strCommand.AppendLine("zCommand.CommandType = CommandType.Text;");

            strCommandCreated_Server.Append(strCommand.ToString());
            strCommandCreated_Client.Append(strCommand.ToString());
            strCommandUpdate.Append(strCommand.ToString());

            for (int i = 0; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                zType = LVColumn.Items[i].SubItems[2].Text;
                if (zFieldName != "CreatedOn" && zFieldName != "ModifiedOn")
                {
                    StringBuilder zOneTypeData = new StringBuilder();
                    switch (zType)
                    {
                        case "char":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Char).Value = " + _strClassName + zFieldName + ";");
                            break;

                        case "nchar":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.NChar).Value = " + _strClassName + zFieldName + ";");
                            break;
                        case "nvarchar":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.NVarChar).Value = " + _strClassName + zFieldName + ";");
                            break;
                        case "ntext":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.NText).Value = " + _strClassName + zFieldName + ";");
                            break;
                        case "int":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Int).Value = " + _strClassName + zFieldName + ";");
                            break;
                        case "date":
                            zOneTypeData.AppendLine("if (" + _strClassName + zFieldName + " == DateTime.MinValue) ");
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Date).Value = DBNull.Value;");
                            zOneTypeData.AppendLine("else");
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Date).Value = " + _strClassName + zFieldName + ";");
                            break;
                        case "datetime":
                            zOneTypeData.AppendLine("if (" + _strClassName + zFieldName + " == DateTime.MinValue) ");
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.DateTime).Value = DBNull.Value;");
                            zOneTypeData.AppendLine("else");
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.DateTime).Value = " + _strClassName + zFieldName + ";");
                            break;
                        case "time":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Time).Value = " + _strClassName + zFieldName + ";");
                            break;
                        case "money":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Money).Value = " + _strClassName + zFieldName + ";");
                            break;
                        case "bit":
                            zOneTypeData.AppendLine("if (" + _strClassName + zFieldName + " == null) ");
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Bit).Value = DBNull.Value;");
                            zOneTypeData.AppendLine("else");
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Bit).Value = " + _strClassName + zFieldName + ";");
                            break;
                        case "float":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Float).Value = " + _strClassName + zFieldName + ";");
                            break;
                        case "numeric":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Decimal).Value = " + _strClassName + zFieldName + ";");
                            break;
                        case "uniqueidentifier":
                            zOneTypeData.AppendLine("if(" + _strClassName + zFieldName + " != \"\" && " + _strClassName + zFieldName + ".Length == 36)");
                            zOneTypeData.AppendLine("{");
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.UniqueIdentifier).Value = Guid.Parse(" + _strClassName + zFieldName + ");");
                            zOneTypeData.AppendLine("}");
                            zOneTypeData.AppendLine("else");
                            zOneTypeData.AppendLine("{");
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.UniqueIdentifier).Value = DBNull.Value;");
                            zOneTypeData.AppendLine("}");
                            break;
                        case "bigint":
                            strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                            strCommand.Append(_strClassName + zFieldName + " = long.Parse(zReader[\"" + zFieldName + "\"].ToString());").AppendLine();
                            break;
                    }
                    if (i > 0)
                    {
                        strCommandCreated_Server.Append(zOneTypeData.ToString());
                    }

                    if (zFieldName != "CreatedBy" && zFieldName != "CreatedName")
                    {
                        strCommandUpdate.Append(zOneTypeData.ToString());
                    }

                    strCommandCreated_Client.Append(zOneTypeData.ToString());
                }
            }

            string[] zOneLine = new string[20];
            zOneLine[0] = "zResult = zCommand.ExecuteNonQuery().ToString();";
            zOneLine[1] = "zCommand.Dispose();";
            zOneLine[2] = "_Message = \"201 Created\";";
            zOneLine[3] = "_Message = \"200 OK\";";
            zOneLine[4] = "}";
            zOneLine[5] = "catch (Exception Err)";
            zOneLine[6] = "{";
            zOneLine[7] = "_Message = \"500 \" + Err.ToString();";
            zOneLine[8] = "}";
            zOneLine[9] = " finally";
            zOneLine[10] = "{";
            zOneLine[11] = "zConnect.Close();";
            zOneLine[12] = "}";
            zOneLine[13] = "return zResult;";
            zOneLine[14] = "}";

            strCommandCreated_Server.AppendLine(zOneLine[0]);
            strCommandCreated_Server.AppendLine(zOneLine[1]);
            strCommandCreated_Server.AppendLine(zOneLine[2]);

            strCommandCreated_Client.AppendLine(zOneLine[0]);
            strCommandCreated_Client.AppendLine(zOneLine[1]);
            strCommandCreated_Client.AppendLine(zOneLine[2]);

            strCommandUpdate.AppendLine(zOneLine[0]);
            strCommandUpdate.AppendLine(zOneLine[1]);
            strCommandUpdate.AppendLine(zOneLine[3]);

            for (int i = 4; i < 15; i++)
            {
                strCommandCreated_Server.AppendLine(zOneLine[i]);
                strCommandCreated_Client.AppendLine(zOneLine[i]);
                strCommandUpdate.AppendLine(zOneLine[i]);
            }

            w.WriteLine(strCommandCreated_Server);


            w.WriteLine(strCommandCreated_Client);

            // UPDATE
            w.WriteLine("public string Update() ");
            w.WriteLine("{ ");
            w.WriteLine("string zSQL = \"UPDATE " + _strTableName + " SET \" ");
            strField = "";
            for (int i = 1; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                zType = LVColumn.Items[i].SubItems[2].Text;
                if (zFieldName != "CreatedOn" && zFieldName != "CreatedBy" && zFieldName != "CreatedName")
                {
                    if (zFieldName == "ModifiedOn")
                    {
                        strField = "            + \" " + zFieldName + " = GetDate()";
                    }
                    else
                    {
                        strField = "            + \" " + zFieldName + " = @" + zFieldName;
                    }

                    if (i < n - 1)
                    {
                        strField += ",\"";
                    }
                    else
                    {
                        strField += "\"";
                    }

                    w.WriteLine(strField);
                }
            }
            w.WriteLine("            + \" WHERE " + zKeyName + " = @" + zKeyName + "\";");
            w.WriteLine(strCommandUpdate);


            #endregion

            #region [ Delete ]
            w.WriteLine("public string Delete()");
            w.WriteLine("{");
            w.WriteLine("string zResult = \"\";");

            w.WriteLine("//---------- String SQL Access Database ---------------");
            w.WriteLine("string zSQL = \"UPDATE " + _strTableName + " SET RecordStatus = 99 WHERE " + zKeyName + " = @" + zKeyName + "\";");
            w.WriteLine("string zConnectionString = " + cbo_Connect.Text + ";");
            w.WriteLine("SqlConnection zConnect = new SqlConnection(zConnectionString);");
            w.WriteLine("zConnect.Open();");
            w.WriteLine("try");
            w.WriteLine("{");
            w.WriteLine("SqlCommand zCommand = new SqlCommand(zSQL, zConnect);");
            if (zKeyTypeSQL == "SqlDbType.UniqueIdentifier")
            {
                w.WriteLine("zCommand.Parameters.Add(\"@" + zKeyName + "\", " + zKeyTypeSQL + ").Value = Guid.Parse(_" + zKeyName + ");");
            }
            else
            {
                w.WriteLine("zCommand.Parameters.Add(\"@" + zKeyName + "\", " + zKeyTypeSQL + ").Value = _" + zKeyName + ";");
            }

            w.WriteLine("zResult = zCommand.ExecuteNonQuery().ToString();");
            w.WriteLine("zCommand.Dispose();");
            w.WriteLine("_Message = \"200 OK\";");
            w.WriteLine("}");
            w.WriteLine("catch (Exception Err)");
            w.WriteLine("{");
            w.WriteLine("_Message = \"500\" + Err.ToString();");
            w.WriteLine("}");
            w.WriteLine("finally");
            w.WriteLine("{");
            w.WriteLine("zConnect.Close();");
            w.WriteLine("}");
            w.WriteLine("return zResult;");
            w.WriteLine("}");
            #endregion

            #region [ Empty ]
            w.WriteLine("public string Empty()");
            w.WriteLine("{");
            w.WriteLine("string zResult = \"\";");

            w.WriteLine("//---------- String SQL Access Database ---------------");
            w.WriteLine("string zSQL = \"DELETE FROM " + _strTableName + " WHERE " + zKeyName + " = @" + zKeyName + "\";");
            w.WriteLine("string zConnectionString = " + cbo_Connect.Text + ";");
            w.WriteLine("SqlConnection zConnect = new SqlConnection(zConnectionString);");
            w.WriteLine("zConnect.Open();");
            w.WriteLine("try");
            w.WriteLine("{");
            w.WriteLine("SqlCommand zCommand = new SqlCommand(zSQL, zConnect);");
            if (zKeyTypeSQL == "SqlDbType.UniqueIdentifier")
            {
                w.WriteLine("zCommand.Parameters.Add(\"@" + zKeyName + "\", " + zKeyTypeSQL + ").Value = Guid.Parse(_" + zKeyName + ");");
            }
            else
            {
                w.WriteLine("zCommand.Parameters.Add(\"@" + zKeyName + "\", " + zKeyTypeSQL + ").Value = _" + zKeyName + ";");
            }

            w.WriteLine("zResult = zCommand.ExecuteNonQuery().ToString();");
            w.WriteLine("zCommand.Dispose();");
            w.WriteLine("_Message = \"200 OK\";");
            w.WriteLine("}");
            w.WriteLine("catch (Exception Err)");
            w.WriteLine("{");
            w.WriteLine("_Message = \"500\" + Err.ToString();");
            w.WriteLine("}");
            w.WriteLine("finally");
            w.WriteLine("{");
            w.WriteLine("zConnect.Close();");
            w.WriteLine("}");
            w.WriteLine("return zResult;");
            w.WriteLine("}");

            #endregion

            w.WriteLine("#endregion");

            w.WriteLine("}");// end class

            // end namespace
            w.WriteLine("}");

            // Bảo đảm tất cả dữ liệu được ghi từ buffer.
            w.Flush();
            // Đóng file.
            w.Close();
            fs.Close();

        }
        private void CreateFileInfoV1(string In_FileName)
        {
            FileStream fs = new FileStream(In_FileName, FileMode.Create);
            StreamWriter w = new StreamWriter(fs, Encoding.UTF8);

            string _strClassName = _strTableName.Remove(0, 4);

            int n = LVColumn.Items.Count;
            string zKeyName = "";
            string zKeyTypeSQL = "";
            string zKeyTypeCShap = "";
            string zFieldName = "";
            string zType = "";
            string strField = "";
            string zPrimary = "";

            w.WriteLine("using System.Configuration;");
            w.WriteLine("using System;");
            w.WriteLine("using System.Collections.Generic;");
            w.WriteLine("using System.Text;");
            w.WriteLine("using System.Data;");
            w.WriteLine("using System.Data.SqlClient;");
            if (txt_Using.Text.Trim().Length > 0)
            {
                w.WriteLine("using " + txt_Using.Text.Trim() + ";");
            }
            w.WriteLine("namespace " + txt_NameSpace.Text);
            w.WriteLine("{");


            w.WriteLine("public partial class " + _strClassName + "_Info");
            w.WriteLine("{");

            #region [Data type]
            w.WriteLine("#region [ Field Name ]");
            for (int i = 0; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                zType = LVColumn.Items[i].SubItems[2].Text;
                zPrimary = LVColumn.Items[i].SubItems[5].Text;
                switch (zType)
                {
                    case "uniqueidentifier":
                        w.WriteLine("private string _" + zFieldName + " = \"\";");
                        if (i == 0 && zPrimary == "NO")
                        {
                            zKeyTypeSQL = "SqlDbType.UniqueIdentifier";
                            zKeyTypeCShap = "string";
                            zKeyName = zFieldName;
                        }
                        break;
                    case "char":
                        w.WriteLine("private string _" + zFieldName + " = \"\";");
                        if (i == 0)
                        {
                            zKeyTypeSQL = "SqlDbType.Char";
                            zKeyTypeCShap = "string";
                            zKeyName = zFieldName;
                        }
                        break;

                    case "nchar":
                        w.WriteLine("private string _" + zFieldName + " = \"\";");
                        if (i == 0)
                        {
                            zKeyTypeSQL = "SqlDbType.NChar";
                            zKeyTypeCShap = "string";
                            zKeyName = zFieldName;
                        }
                        break;
                    case "nvarchar":
                        w.WriteLine("private string _" + zFieldName + " = \"\";");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.NVarChar";
                            zKeyTypeCShap = "string";
                        }
                        break;
                    case "ntext":
                        w.WriteLine("private string _" + zFieldName + " = \"\";");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.NText";
                            zKeyTypeCShap = "string";
                        }
                        break;
                    case "int":
                        w.WriteLine("private int _" + zFieldName + " = 0;");
                        if (i == 0 && zPrimary == "NO")
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Int";
                            zKeyTypeCShap = "int";
                        }
                        break;
                    case "date":
                        w.WriteLine("private DateTime _" + zFieldName + " = DateTime.MinValue;");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Date";
                            zKeyTypeCShap = "DateTime";
                        }
                        break;
                    case "datetime":
                        w.WriteLine("private DateTime _" + zFieldName + " = DateTime.MinValue;");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.DateTime";
                            zKeyTypeCShap = "DateTime";
                        }
                        break;
                    case "time":
                        w.WriteLine("private TimeSpan _" + zFieldName + " = new TimeSpan();");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Time";
                            zKeyTypeCShap = "TimeSpan";
                        }
                        break;
                    case "money":
                        w.WriteLine("private double _" + zFieldName + " = 0;");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Money";
                            zKeyTypeCShap = "double";
                        }
                        break;
                    case "bit":
                        w.WriteLine("private bool _" + zFieldName + ";");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Bit";
                            zKeyTypeCShap = "bool";
                        }
                        break;
                    case "image":
                        w.WriteLine("private Image _" + zFieldName + ";");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Image";
                            zKeyTypeCShap = "Image";
                        }
                        break;

                    case "float":
                        w.WriteLine("private float _" + zFieldName + "= 0;");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Float";
                            zKeyTypeCShap = "float";
                        }
                        break;
                    case "numeric":
                        w.WriteLine("private long _" + zFieldName + "= 0;");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.Float";
                            zKeyTypeCShap = "float";
                        }
                        break;
                    case "bigint":
                        w.WriteLine("private long _" + zFieldName + "= 0;");
                        if (i == 0)
                        {
                            zKeyName = zFieldName;
                            zKeyTypeSQL = "SqlDbType.BigInt";
                            zKeyTypeCShap = "long";
                        }
                        break;
                }
            }
            w.WriteLine("#endregion");
            #endregion

            #region [ Properties ]

            w.WriteLine("#region [ Properties ]");
            // BEGIN Properties
            for (int i = 0; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                zType = LVColumn.Items[i].SubItems[2].Text;

                switch (zType)
                {
                    case "char":
                        w.WriteLine("public string " + zFieldName);
                        break;
                    case "nchar":
                        w.WriteLine("public string " + zFieldName);
                        break;
                    case "nvarchar":
                        w.WriteLine("public string " + zFieldName);
                        break;
                    case "ntext":
                        w.WriteLine("public string " + zFieldName);
                        break;
                    case "int":
                        w.WriteLine("public int " + zFieldName);
                        break;
                    case "date":
                        w.WriteLine("public DateTime " + zFieldName);
                        break;
                    case "datetime":
                        w.WriteLine("public DateTime " + zFieldName);
                        break;
                    case "time":
                        w.WriteLine("public TimeSpan " + zFieldName);
                        break;
                    case "money":
                        w.WriteLine("public double " + zFieldName);
                        break;
                    case "bit":
                        w.WriteLine("public bool " + zFieldName);
                        break;
                    case "image":
                        w.WriteLine("public Image " + zFieldName);
                        break;
                    case "float":
                        w.WriteLine("public float " + zFieldName);
                        break;
                    case "numeric":
                        w.WriteLine("public float " + zFieldName);
                        break;
                    case "uniqueidentifier":
                        w.WriteLine("public string " + zFieldName);
                        break;
                    case "bigint":
                        w.WriteLine("public long " + zFieldName);
                        break;
                }
                w.WriteLine("{");
                w.WriteLine("get { return _" + zFieldName + "; }");
                w.WriteLine("set { _" + zFieldName + " = value; }");
                w.WriteLine("}");

            }


            w.WriteLine("#endregion");
            #endregion

            w.WriteLine("#region [ ExtraInfo ]");
            w.WriteLine("private string _Message = \"\";");
            w.WriteLine("public int Code ");
            w.WriteLine("{");
            w.WriteLine("get { ");
            w.WriteLine("if (_Message.Length >= 3)");
            w.WriteLine("return int.Parse(_Message.Substring(0, 3));");
            w.WriteLine("else return -1;");
            w.WriteLine("}");
            w.WriteLine("}");
            w.WriteLine("public string Message ");
            w.WriteLine("{");
            w.WriteLine("get { return _Message; }");
            w.WriteLine("set { _Message = value; }");
            w.WriteLine("}");
            w.WriteLine("#endregion");
            _strClassName += ".";

            #region [ Constructor Get Information ]

            w.WriteLine("#region [ Constructor Get Information ]");
            //Begin Class Info
            w.WriteLine("public " + _strClassName.Replace(".", "") + "_Info()");
            w.WriteLine("{");
            if (zKeyTypeSQL == "SqlDbType.UniqueIdentifier")
            {
                w.WriteLine(_strClassName + zKeyName + " = Guid.NewGuid().ToString();");
            }
            w.WriteLine("}");

            w.WriteLine("public " + _strClassName.Replace(".", "") + "_Info(" + zKeyTypeCShap + " " + zKeyName + ")");
            w.WriteLine("{");
            w.WriteLine("string zSQL = \"SELECT * FROM " + _strTableName + " WHERE " + zKeyName + " = @" + zKeyName + " AND RecordStatus != 99 \"; ");

            w.WriteLine("string zConnectionString = " + cbo_Connect.Text + ";");
            w.WriteLine("SqlConnection zConnect = new SqlConnection(zConnectionString);");
            w.WriteLine("zConnect.Open();");
            w.WriteLine("try");
            w.WriteLine("{");
            w.WriteLine("SqlCommand zCommand = new SqlCommand(zSQL, zConnect);");
            w.WriteLine("zCommand.CommandType = CommandType.Text;");
            if (zKeyTypeSQL == "SqlDbType.UniqueIdentifier")
            {
                w.WriteLine("zCommand.Parameters.Add(\"@" + zKeyName + "\", " + zKeyTypeSQL + ").Value = Guid.Parse(" + zKeyName + ");");
            }
            else
            {
                w.WriteLine("zCommand.Parameters.Add(\"@" + zKeyName + "\", " + zKeyTypeSQL + ").Value = " + zKeyName + ";");
            }

            w.WriteLine("SqlDataReader zReader = zCommand.ExecuteReader();");
            w.WriteLine("if (zReader.HasRows)");
            w.WriteLine("{");
            w.WriteLine("zReader.Read();");
            StringBuilder strCommand = new StringBuilder();
            StringBuilder strCommandCreated_Server = new StringBuilder();
            StringBuilder strCommandCreated_Client = new StringBuilder();
            StringBuilder strCommandUpdate = new StringBuilder();

            for (int i = 0; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                zType = LVColumn.Items[i].SubItems[2].Text;

                switch (zType)
                {
                    case "char":
                        strCommand.Append("_" + zFieldName + " = zReader[\"" + zFieldName + "\"].ToString();").AppendLine();
                        break;
                    case "nchar":
                        strCommand.Append("_" + zFieldName + " = zReader[\"" + zFieldName + "\"].ToString();").AppendLine();
                        break;
                    case "nvarchar":
                        strCommand.Append("_" + zFieldName + " = zReader[\"" + zFieldName + "\"].ToString();").AppendLine();
                        break;
                    case "ntext":
                        strCommand.Append("_" + zFieldName + " = zReader[\"" + zFieldName + "\"].ToString();").AppendLine();
                        break;
                    case "int":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append("_" + zFieldName + " = int.Parse(zReader[\"" + zFieldName + "\"].ToString());").AppendLine();
                        break;
                    case "date":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append("_" + zFieldName + " = (DateTime)zReader[\"" + zFieldName + "\"];").AppendLine();
                        break;
                    case "datetime":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append("_" + zFieldName + " = (DateTime)zReader[\"" + zFieldName + "\"];").AppendLine();
                        break;
                    case "time":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append("_" + zFieldName + " = (TimeSpan)zReader[\"" + zFieldName + "\"];").AppendLine();
                        break;
                    case "money":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append("_" + zFieldName + " = double.Parse(zReader[\"" + zFieldName + "\"].ToString());").AppendLine();
                        break;
                    case "bit":
                        strCommand.Append("_" + zFieldName + " = (bool)zReader[\"" + zFieldName + "\"];").AppendLine();
                        break;
                    case "image":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append("_" + zFieldName + " = (Image)zReader[\"" + zFieldName + "\"];").AppendLine();
                        break;

                    case "float":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append("_" + zFieldName + " = float.Parse(zReader[\"" + zFieldName + "\"].ToString());").AppendLine();
                        break;
                    case "numeric":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append("_" + zFieldName + " = float.Parse(zReader[\"" + zFieldName + "\"].ToString());").AppendLine();
                        break;
                    case "uniqueidentifier":
                        strCommand.Append("_" + zFieldName + " = zReader[\"" + zFieldName + "\"].ToString();").AppendLine();
                        break;

                    case "bigint":
                        strCommand.Append("if (zReader[\"" + zFieldName + "\"]!= DBNull.Value)").AppendLine();
                        strCommand.Append("_" + zFieldName + " = long.Parse(zReader[\"" + zFieldName + "\"].ToString());").AppendLine();
                        break;
                }
            }
            strCommand.AppendLine("_Message = \"200 OK\";");
            strCommand.AppendLine("}");
            strCommand.AppendLine("else");
            strCommand.AppendLine("{");
            strCommand.AppendLine("_Message = \"404 Not Found\";");
            strCommand.AppendLine("}");
            strCommand.AppendLine("zReader.Close();");
            strCommand.AppendLine("zCommand.Dispose();");
            strCommand.AppendLine("}");
            strCommand.AppendLine("catch (Exception Err)");
            strCommand.AppendLine("{");
            strCommand.AppendLine("_Message = \"500 \" + Err.ToString();");
            strCommand.AppendLine("}");
            strCommand.AppendLine("finally");
            strCommand.AppendLine("{");
            strCommand.AppendLine("zConnect.Close();");
            strCommand.AppendLine("}");
            strCommand.AppendLine("}");

            w.WriteLine(strCommand.ToString());
            w.WriteLine("#endregion");
            #endregion



            #region [ Constructor Create - Update Information ]
            w.WriteLine("#region [ Constructor Update Information ]");
            strCommand = new StringBuilder();
            strCommandCreated_Server = new StringBuilder();
            strCommandCreated_Client = new StringBuilder();
            strCommandUpdate = new StringBuilder();

            #region [ Create Key On Server ]
            strCommandCreated_Server.AppendLine("public string Create_KeyAuto()");
            strCommandCreated_Server.AppendLine("{");

            strCommandCreated_Server.AppendLine("//---------- String SQL Access Database ---------------");
            strCommandCreated_Server.AppendLine("    string zSQL = \"INSERT INTO " + _strTableName + " (\" ");
            strField = " + \" ";
            for (int i = 1; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                zType = LVColumn.Items[i].SubItems[2].Text;
                if (zFieldName != "CreatedOn" && zFieldName != "ModifiedOn")
                {
                    strField += zFieldName + " ";
                    if (i < n - 1)
                    {
                        strField += ", ";
                    }
                    else
                    {
                        strField += ") \"";
                    }
                }
            }
            strCommandCreated_Server.AppendLine(strField);
            strCommandCreated_Server.AppendLine(" + \" VALUES ( \"");

            strField = " + \" ";
            for (int i = 1; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                if (zFieldName != "CreatedOn" && zFieldName != "ModifiedOn")
                {
                    strField += "@" + zFieldName + " ";

                    if (i < n - 1)
                    {
                        strField += ", ";
                    }
                    else
                    {
                        strField += ") \";";
                    }
                }
            }
            strCommandCreated_Server.AppendLine(strField);
            strCommandCreated_Server.AppendLine("zSQL += \"SELECT " + zKeyName + " FROM " + _strTableName + " WHERE " + zKeyName + " = SCOPE_IDENTITY()\";");
            #endregion

            #region [ Create Key at Client]
            strCommandCreated_Client.AppendLine("public string Create_KeyManual()");
            strCommandCreated_Client.AppendLine("{");

            strCommandCreated_Client.AppendLine("//---------- String SQL Access Database ---------------");
            strCommandCreated_Client.AppendLine("    string zSQL = \"INSERT INTO " + _strTableName + "(\" ");
            strField = " + \" ";
            for (int i = 0; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                zType = LVColumn.Items[i].SubItems[2].Text;
                if (zFieldName != "CreatedOn" && zFieldName != "ModifiedOn")
                {
                    strField += zFieldName + " ";
                    if (i < n - 1)
                    {
                        strField += ", ";
                    }
                    else
                    {
                        strField += ") \"";
                    }
                }
            }
            strCommandCreated_Client.AppendLine(strField);
            strCommandCreated_Client.AppendLine(" + \" VALUES ( \"");

            strField = " + \" ";
            for (int i = 0; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                if (zFieldName != "CreatedOn" && zFieldName != "ModifiedOn")
                {
                    strField += "@" + zFieldName + " ";

                    if (i < n - 1)
                    {
                        strField += ", ";
                    }
                    else
                    {
                        strField += ") \";";
                    }
                }
            }
            strCommandCreated_Client.AppendLine(strField);
            #endregion

            strCommand.AppendLine("string zResult = \"\"; ");
            strCommand.AppendLine("string zConnectionString = " + cbo_Connect.Text + ";");
            strCommand.AppendLine("SqlConnection zConnect = new SqlConnection(zConnectionString);");
            strCommand.AppendLine("zConnect.Open();");
            strCommand.AppendLine("try");
            strCommand.AppendLine("{");
            strCommand.AppendLine("SqlCommand zCommand = new SqlCommand(zSQL, zConnect);");
            strCommand.AppendLine("zCommand.CommandType = CommandType.Text;");

            strCommandCreated_Server.Append(strCommand.ToString());
            strCommandCreated_Client.Append(strCommand.ToString());
            strCommandUpdate.Append(strCommand.ToString());

            for (int i = 0; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                zType = LVColumn.Items[i].SubItems[2].Text;
                if (zFieldName != "CreatedOn" && zFieldName != "ModifiedOn")
                {
                    StringBuilder zOneTypeData = new StringBuilder();
                    switch (zType)
                    {
                        case "char":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Char).Value = _" + zFieldName + ";");
                            break;

                        case "nchar":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.NChar).Value = _" + zFieldName + ";");
                            break;
                        case "nvarchar":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.NVarChar).Value = _" + zFieldName + ";");
                            break;
                        case "ntext":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.NText).Value = _" + zFieldName + ";");
                            break;
                        case "int":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Int).Value = _" + zFieldName + ";");
                            break;
                        case "date":
                            zOneTypeData.AppendLine("if (_" + zFieldName + " == DateTime.MinValue) ");
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Date).Value = DBNull.Value;");
                            zOneTypeData.AppendLine("else");
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Date).Value = _" + zFieldName + ";");
                            break;
                        case "datetime":
                            zOneTypeData.AppendLine("if (_" + zFieldName + " == DateTime.MinValue) ");
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.DateTime).Value = DBNull.Value;");
                            zOneTypeData.AppendLine("else");
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.DateTime).Value = _" + zFieldName + ";");
                            break;
                        case "time":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Time).Value = _" + zFieldName + ";");
                            break;
                        case "money":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Money).Value = _" + zFieldName + ";");
                            break;
                        case "bit":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Bit).Value = _" + zFieldName + ";");
                            break;
                        case "float":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Float).Value = _" + zFieldName + ";");
                            break;
                        case "numeric":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.Decimal).Value = _" + zFieldName + ";");
                            break;
                        case "uniqueidentifier":
                            zOneTypeData.AppendLine("if(_" + zFieldName + " != \"\" && _" + zFieldName + ".Length == 36)");
                            zOneTypeData.AppendLine("{");
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_" + zFieldName + ");");
                            zOneTypeData.AppendLine("}");
                            zOneTypeData.AppendLine("else");
                            zOneTypeData.AppendLine("{");
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.UniqueIdentifier).Value = DBNull.Value;");
                            zOneTypeData.AppendLine("}");
                            break;
                        case "bigint":
                            zOneTypeData.AppendLine("zCommand.Parameters.Add(\"@" + zFieldName + "\", SqlDbType.BigInt).Value = _" + zFieldName + ";");
                            break;
                    }
                    if (i > 0)
                    {
                        strCommandCreated_Server.Append(zOneTypeData.ToString());
                    }

                    if (zFieldName != "CreatedBy" && zFieldName != "CreatedName")
                    {
                        strCommandUpdate.Append(zOneTypeData.ToString());
                    }

                    strCommandCreated_Client.Append(zOneTypeData.ToString());
                }
            }

            string[] zOneLine = new string[20];
            zOneLine[15] = "zResult = zCommand.ExecuteNonQuery().ToString();";
            zOneLine[0] = "_" + zKeyName + " = Convert.ToInt64(zCommand.ExecuteScalar());";

            zOneLine[1] = "zCommand.Dispose();";
            zOneLine[2] = "_Message = \"201 Created\";";
            zOneLine[3] = "_Message = \"200 OK\";";
            zOneLine[4] = "}";
            zOneLine[5] = "catch (Exception Err)";
            zOneLine[6] = "{";
            zOneLine[7] = "_Message = \"500 \" + Err.ToString();";
            zOneLine[8] = "}";
            zOneLine[9] = " finally";
            zOneLine[10] = "{";
            zOneLine[11] = "zConnect.Close();";
            zOneLine[12] = "}";
            zOneLine[13] = "return zResult;";
            zOneLine[14] = "}";

            strCommandCreated_Server.AppendLine(zOneLine[0]);
            strCommandCreated_Server.AppendLine(zOneLine[1]);
            strCommandCreated_Server.AppendLine(zOneLine[2]);

            strCommandCreated_Client.AppendLine(zOneLine[15]);
            strCommandCreated_Client.AppendLine(zOneLine[1]);
            strCommandCreated_Client.AppendLine(zOneLine[2]);

            strCommandUpdate.AppendLine(zOneLine[15]);
            strCommandUpdate.AppendLine(zOneLine[1]);
            strCommandUpdate.AppendLine(zOneLine[3]);

            for (int i = 4; i < 15; i++)
            {
                strCommandCreated_Server.AppendLine(zOneLine[i]);
                strCommandCreated_Client.AppendLine(zOneLine[i]);
                strCommandUpdate.AppendLine(zOneLine[i]);
            }

            w.WriteLine(strCommandCreated_Server);
            w.WriteLine(strCommandCreated_Client);
            // UPDATE
            w.WriteLine("public string Update() ");
            w.WriteLine("{ ");
            w.WriteLine("string zSQL = \"UPDATE " + _strTableName + " SET \" ");
            strField = "";
            for (int i = 1; i < n; i++)
            {
                zFieldName = LVColumn.Items[i].SubItems[1].Text;
                zType = LVColumn.Items[i].SubItems[2].Text;
                if (zFieldName != "CreatedOn" && zFieldName != "CreatedBy" && zFieldName != "CreatedName")
                {
                    if (zFieldName == "ModifiedOn")
                    {
                        strField = "            + \" " + zFieldName + " = GetDate()";
                    }
                    else
                    {
                        strField = "            + \" " + zFieldName + " = @" + zFieldName;
                    }

                    if (i < n - 1)
                    {
                        strField += ",\"";
                    }
                    else
                    {
                        strField += "\"";
                    }

                    w.WriteLine(strField);
                }
            }
            w.WriteLine("            + \" WHERE " + zKeyName + " = @" + zKeyName + "\";");
            w.WriteLine(strCommandUpdate);
            #endregion

            #region [ Delete ]
            w.WriteLine("public string Delete()");
            w.WriteLine("{");
            w.WriteLine("string zResult = \"\";");

            w.WriteLine("//---------- String SQL Access Database ---------------");
            w.WriteLine("string zSQL = \"UPDATE " + _strTableName + " SET RecordStatus = 99 WHERE " + zKeyName + " = @" + zKeyName + "\";");
            w.WriteLine("string zConnectionString = " + cbo_Connect.Text + ";");
            w.WriteLine("SqlConnection zConnect = new SqlConnection(zConnectionString);");
            w.WriteLine("zConnect.Open();");
            w.WriteLine("try");
            w.WriteLine("{");
            w.WriteLine("SqlCommand zCommand = new SqlCommand(zSQL, zConnect);");
            if (zKeyTypeSQL == "SqlDbType.UniqueIdentifier")
            {
                w.WriteLine("zCommand.Parameters.Add(\"@" + zKeyName + "\", " + zKeyTypeSQL + ").Value = Guid.Parse(_" + zKeyName + ");");
            }
            else
            {
                w.WriteLine("zCommand.Parameters.Add(\"@" + zKeyName + "\", " + zKeyTypeSQL + ").Value = _" + zKeyName + ";");
            }

            w.WriteLine("zResult = zCommand.ExecuteNonQuery().ToString();");
            w.WriteLine("zCommand.Dispose();");
            w.WriteLine("_Message = \"200 OK\";");
            w.WriteLine("}");
            w.WriteLine("catch (Exception Err)");
            w.WriteLine("{");
            w.WriteLine("_Message = \"500\" + Err.ToString();");
            w.WriteLine("}");
            w.WriteLine("finally");
            w.WriteLine("{");
            w.WriteLine("zConnect.Close();");
            w.WriteLine("}");
            w.WriteLine("return zResult;");
            w.WriteLine("}");
            #endregion

            #region [ Empty ]
            w.WriteLine("public string Empty()");
            w.WriteLine("{");
            w.WriteLine("string zResult = \"\";");

            w.WriteLine("//---------- String SQL Access Database ---------------");
            w.WriteLine("string zSQL = \"DELETE FROM " + _strTableName + " WHERE " + zKeyName + " = @" + zKeyName + "\";");
            w.WriteLine("string zConnectionString = " + cbo_Connect.Text + ";");
            w.WriteLine("SqlConnection zConnect = new SqlConnection(zConnectionString);");
            w.WriteLine("zConnect.Open();");
            w.WriteLine("try");
            w.WriteLine("{");
            w.WriteLine("SqlCommand zCommand = new SqlCommand(zSQL, zConnect);");
            if (zKeyTypeSQL == "SqlDbType.UniqueIdentifier")
            {
                w.WriteLine("zCommand.Parameters.Add(\"@" + zKeyName + "\", " + zKeyTypeSQL + ").Value = Guid.Parse(_" + zKeyName + ");");
            }
            else
            {
                w.WriteLine("zCommand.Parameters.Add(\"@" + zKeyName + "\", " + zKeyTypeSQL + ").Value = _" + zKeyName + ";");
            }

            w.WriteLine("zResult = zCommand.ExecuteNonQuery().ToString();");
            w.WriteLine("zCommand.Dispose();");
            w.WriteLine("_Message = \"200 OK\";");
            w.WriteLine("}");
            w.WriteLine("catch (Exception Err)");
            w.WriteLine("{");
            w.WriteLine("_Message = \"500\" + Err.ToString();");
            w.WriteLine("}");
            w.WriteLine("finally");
            w.WriteLine("{");
            w.WriteLine("zConnect.Close();");
            w.WriteLine("}");
            w.WriteLine("return zResult;");
            w.WriteLine("}");
            #endregion
            w.WriteLine("#endregion");
            w.WriteLine("}");// end class
            // end namespace
            w.WriteLine("}");

            // Bảo đảm tất cả dữ liệu được ghi từ buffer.
            w.Flush();
            // Đóng file.
            w.Close();
            fs.Close();

        }
        #endregion

        DataTable ReadCSV(string FilePath)
        {
            string Fulltext;
            DataTable dtCsv = new DataTable();

            using (StreamReader sr = new StreamReader(FilePath))
            {
                while (!sr.EndOfStream)
                {
                    Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                    string[] rows = Fulltext.Split('\n'); //split full file text into rows  
                    for (int i = 0; i < rows.Count() - 1; i++)
                    {
                        string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  
                        {
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dtCsv.Columns.Add(rowValues[j]); //add headers  
                                }
                            }
                            else
                            {
                                DataRow dr = dtCsv.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    dr[k] = rowValues[k].ToString();
                                }
                                dtCsv.Rows.Add(dr); //add other rows  
                            }
                        }
                    }
                }
            }

            return dtCsv;
        }
        void WriteCSV(DataTable dtDataTable, string strFilePath)
        {
            StreamWriter sw = new StreamWriter(strFilePath, false);
            //headers  
            for (int i = 0; i < dtDataTable.Columns.Count; i++)
            {
                sw.Write(dtDataTable.Columns[i]);
                if (i < dtDataTable.Columns.Count - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);
            foreach (DataRow dr in dtDataTable.Rows)
            {
                for (int i = 0; i < dtDataTable.Columns.Count; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        string value = dr[i].ToString();
                        if (value.Contains(','))
                        {
                            value = String.Format("\"{0}\"", value);
                            sw.Write(value);
                        }
                        else
                        {
                            sw.Write(dr[i].ToString());
                        }
                    }
                    if (i < dtDataTable.Columns.Count - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();
        }

        private void AutoStringBuilder()
        {
            string txt = txt_Before.Text;
            string[] lst = txt.Split(new Char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string line in lst)
            {
                txt_After.Text += "zSb.AppendLine(\"" + line.Replace("\"", "'").Trim() + "\");" + "\r\n";
            }
        }

        private void btnMini_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}

/// <summary>
/// 
/// </summary>
public class Access_Data
{
    public static string _ConnectString = "";
    public static DataTable GetListTable()
    {
        DataTable nTable = new DataTable();
        string strSQL = "SELECT  * FROM information_schema.TABLE_CONSTRAINTS ORDER BY TABLE_NAME";
        SqlConnection zSQLServer = new SqlConnection();
        zSQLServer = new SqlConnection(_ConnectString);

        try
        {
            SqlCommand _Command = new SqlCommand(strSQL, zSQLServer);
            SqlDataAdapter nStudents = new SqlDataAdapter(_Command);

            nStudents.Fill(nTable);
            //---- Close Connect SQL ----
            _Command.Dispose();
        }
        catch (Exception ex)
        {
            string strMessage = ex.ToString();
        }

        return nTable;
    }
    public static DataTable StructOfTable(string TableName)
    {
        DataTable nTable = new DataTable();
        string strSQL = " SELECT * FROM information_schema.COLUMNS"
                      + " WHERE Table_Name = @Table_Name";
        try
        {
            SqlConnection zSQLServer = new SqlConnection();
            zSQLServer = new SqlConnection(_ConnectString);

            SqlCommand _Command = new SqlCommand(strSQL, zSQLServer);
            SqlDataAdapter nStudents = new SqlDataAdapter(_Command);
            _Command.Parameters.Add("@Table_Name", SqlDbType.NVarChar).Value = TableName;
            nStudents.Fill(nTable);
            //---- Close Connect SQL ----
            _Command.Dispose();
        }
        catch (Exception ex)
        {
            string strMessage = ex.ToString();
        }

        return nTable;
    }
    public static DataTable GetDatabase(string Server, string UserName, string Password)
    {
        SqlConnection zCon = new SqlConnection();
        zCon.ConnectionString = "Server=" + Server + ";database=master; User=" + UserName + ";Password= " + Password;
        zCon.Open();
        DataTable zTable = new DataTable();
        string sql = "select name from sys.databases ORDER BY name ASC";
        SqlCommand zCMD = new SqlCommand(sql, zCon);
        zCMD.CommandType = CommandType.Text;
        SqlDataAdapter sqlda = new SqlDataAdapter(zCMD);
        sqlda.Fill(zTable);
        sqlda.Dispose();
        zCMD.Dispose();
        zCon.Close();
        return zTable;
    }
}

/// <summary>
/// 
/// </summary>
public static class Utils
{
    public static void SizeLastColumn_LV(ListView lv)
    {
        if (lv.Columns.Count > 0)
        {
            lv.Columns[lv.Columns.Count - 1].Width = -2;
        }
    }

    #region [Paint LV]
    private static Color _LVForceColor = Color.Navy;                // = Color.LightGreen;//For hover backcolor
    private static Color _LVbackColor = Color.FromArgb(181, 213, 255);// = Color.LightSkyBlue;    //For backColor    

    public static void DrawLVStyle(ref ListView list)
    {
        list.OwnerDraw = true;

        list.DrawColumnHeader +=
            new DrawListViewColumnHeaderEventHandler
            (
                (sender, e) => headerDraw(sender, e, _LVbackColor, _LVForceColor)
            );
        list.DrawItem += new DrawListViewItemEventHandler(bodyDraw);
    }

    private static void headerDraw(object sender, DrawListViewColumnHeaderEventArgs e, Color backColor, Color foreColor)
    {
        ListView lv = (ListView)sender;

        int topHeight = 10;
        Rectangle topRect = new Rectangle(e.Bounds.Left, e.Bounds.Top + 1, e.Bounds.Width, topHeight);
        RectangleF bottomRect = new RectangleF(e.Bounds.Left, e.Bounds.Top + 1 + topHeight, e.Bounds.Width, e.Bounds.Height - topHeight - 4);
        Color c1 = Color.FromArgb(180, backColor);
        using (SolidBrush brush = new SolidBrush(c1))
        {
            e.Graphics.FillRectangle(brush, topRect);
            brush.Color = backColor;
            e.Graphics.FillRectangle(brush, bottomRect);
        }

        e.Graphics.DrawRectangle(new Pen(Color.White, 0.1f), e.Bounds.Left + 0.5f, e.Bounds.Top + 0.5f, e.Bounds.Width - 1f, e.Bounds.Height - 1f);
        ControlPaint.DrawBorder(e.Graphics, e.Bounds,
            Color.Gray, 0, ButtonBorderStyle.Solid,
            Color.Gray, 0, ButtonBorderStyle.Solid,
            Color.Gray, e.ColumnIndex != lv.Columns.Count - 1 ? 1 : 0, ButtonBorderStyle.Solid,
            Color.Gray, 1, ButtonBorderStyle.Solid);

        using (SolidBrush foreBrush = new SolidBrush(foreColor))
        {
            using (StringFormat sf = new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center })
            {
                e.Graphics.DrawString(e.Header.Text, new Font("Tahoma", 9, FontStyle.Bold), foreBrush, e.Bounds, sf);

            }

        }
    }

    private static void bodyDraw(object sender, DrawListViewItemEventArgs e)
    {
        e.DrawDefault = true;
    }

    #endregion
}
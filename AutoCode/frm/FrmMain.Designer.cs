﻿namespace AutoCode
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.label1 = new System.Windows.Forms.Label();
            this.txtTitle = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.cbo_Server = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbo_Password = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.cbo_UserName = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.btnConnect = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.pnTable = new System.Windows.Forms.Panel();
            this.LVTable = new System.Windows.Forms.ListView();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.pnDatabase = new System.Windows.Forms.Panel();
            this.LVDatabase = new System.Windows.Forms.ListView();
            this.kryptonHeader3 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.LVColumn = new System.Windows.Forms.ListView();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_Aspx_Edit = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Aspx_List = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_InfoClass = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_DataClass = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_ModelClass = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Using = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_NameSpace = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.cbo_Connect = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chkData = new System.Windows.Forms.CheckBox();
            this.chk_Edit = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chk_List = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkInfo = new System.Windows.Forms.CheckBox();
            this.chkModel = new System.Windows.Forms.CheckBox();
            this.btnAutoCode = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.txt_Path = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.btnOpenFolder = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.label5 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.txt_Before = new System.Windows.Forms.TextBox();
            this.kryptonHeader4 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btn_AutoString = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.txt_After = new System.Windows.Forms.TextBox();
            this.kryptonHeader5 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btn_Clear = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Server)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Password)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_UserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.pnTable.SuspendLayout();
            this.pnDatabase.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Connect)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label1.Location = new System.Drawing.Point(15, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 22);
            this.label1.TabIndex = 13;
            this.label1.Text = "Tên máy chủ";
            // 
            // txtTitle
            // 
            this.txtTitle.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnClose});
            this.txtTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTitle.Location = new System.Drawing.Point(0, 0);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtTitle.Size = new System.Drawing.Size(1366, 44);
            this.txtTitle.TabIndex = 148;
            this.txtTitle.Values.Description = "";
            this.txtTitle.Values.Heading = "Tự động tạo code, nhập, sữa, xóa";
            this.txtTitle.Values.Image = ((System.Drawing.Image)(resources.GetObject("txtTitle.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Text = "Mini";
            this.btnMini.UniqueName = "37468333A9CF4B91F3BE9504AE96EBDF";
            this.btnMini.Click += new System.EventHandler(this.btnMini_Click_1);
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "45ADC12A75C44720A8985047BFDA9CE1";
            // 
            // cbo_Server
            // 
            this.cbo_Server.DropDownWidth = 117;
            this.cbo_Server.Location = new System.Drawing.Point(136, 10);
            this.cbo_Server.Name = "cbo_Server";
            this.cbo_Server.Size = new System.Drawing.Size(160, 29);
            this.cbo_Server.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label2.Location = new System.Drawing.Point(302, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 22);
            this.label2.TabIndex = 15;
            this.label2.Text = "Tên đăng nhập";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label3.Location = new System.Drawing.Point(574, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 22);
            this.label3.TabIndex = 112;
            this.label3.Text = "Mật khẩu";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cbo_Password);
            this.panel2.Controls.Add(this.cbo_UserName);
            this.panel2.Controls.Add(this.btnConnect);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.cbo_Server);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1352, 67);
            this.panel2.TabIndex = 208;
            // 
            // cbo_Password
            // 
            this.cbo_Password.DropDownWidth = 117;
            this.cbo_Password.Location = new System.Drawing.Point(663, 10);
            this.cbo_Password.Name = "cbo_Password";
            this.cbo_Password.Size = new System.Drawing.Size(117, 29);
            this.cbo_Password.TabIndex = 2;
            // 
            // cbo_UserName
            // 
            this.cbo_UserName.DropDownWidth = 117;
            this.cbo_UserName.Location = new System.Drawing.Point(438, 10);
            this.cbo_UserName.Name = "cbo_UserName";
            this.cbo_UserName.Size = new System.Drawing.Size(117, 29);
            this.cbo_UserName.TabIndex = 1;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(1173, 10);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            this.btnConnect.Size = new System.Drawing.Size(158, 25);
            this.btnConnect.TabIndex = 3;
            this.btnConnect.Values.Text = "Tải tên cơ sở dữ liệu";
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 70);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.pnTable);
            this.splitContainer1.Panel1.Controls.Add(this.pnDatabase);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.LVColumn);
            this.splitContainer1.Panel2.Controls.Add(this.kryptonHeader2);
            this.splitContainer1.Size = new System.Drawing.Size(1352, 491);
            this.splitContainer1.SplitterDistance = 448;
            this.splitContainer1.TabIndex = 209;
            // 
            // pnTable
            // 
            this.pnTable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.pnTable.Controls.Add(this.LVTable);
            this.pnTable.Controls.Add(this.kryptonHeader1);
            this.pnTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTable.Location = new System.Drawing.Point(0, 137);
            this.pnTable.Name = "pnTable";
            this.pnTable.Size = new System.Drawing.Size(446, 352);
            this.pnTable.TabIndex = 209;
            // 
            // LVTable
            // 
            this.LVTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVTable.Font = new System.Drawing.Font("Tahoma", 10F);
            this.LVTable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVTable.FullRowSelect = true;
            this.LVTable.GridLines = true;
            this.LVTable.HideSelection = false;
            this.LVTable.Location = new System.Drawing.Point(0, 30);
            this.LVTable.Name = "LVTable";
            this.LVTable.Size = new System.Drawing.Size(446, 322);
            this.LVTable.TabIndex = 0;
            this.LVTable.UseCompatibleStateImageBehavior = false;
            this.LVTable.View = System.Windows.Forms.View.Details;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(446, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.TabIndex = 200;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "2. Bảng";
            // 
            // pnDatabase
            // 
            this.pnDatabase.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.pnDatabase.Controls.Add(this.LVDatabase);
            this.pnDatabase.Controls.Add(this.kryptonHeader3);
            this.pnDatabase.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnDatabase.Location = new System.Drawing.Point(0, 0);
            this.pnDatabase.Name = "pnDatabase";
            this.pnDatabase.Size = new System.Drawing.Size(446, 137);
            this.pnDatabase.TabIndex = 208;
            // 
            // LVDatabase
            // 
            this.LVDatabase.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVDatabase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVDatabase.Font = new System.Drawing.Font("Tahoma", 10F);
            this.LVDatabase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVDatabase.FullRowSelect = true;
            this.LVDatabase.GridLines = true;
            this.LVDatabase.HideSelection = false;
            this.LVDatabase.Location = new System.Drawing.Point(0, 30);
            this.LVDatabase.Name = "LVDatabase";
            this.LVDatabase.Size = new System.Drawing.Size(446, 107);
            this.LVDatabase.TabIndex = 0;
            this.LVDatabase.UseCompatibleStateImageBehavior = false;
            this.LVDatabase.View = System.Windows.Forms.View.Details;
            // 
            // kryptonHeader3
            // 
            this.kryptonHeader3.AutoSize = false;
            this.kryptonHeader3.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader3.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader3.Name = "kryptonHeader3";
            this.kryptonHeader3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader3.Size = new System.Drawing.Size(446, 30);
            this.kryptonHeader3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader3.TabIndex = 200;
            this.kryptonHeader3.Values.Description = "";
            this.kryptonHeader3.Values.Heading = "1. Dữ liệu";
            // 
            // LVColumn
            // 
            this.LVColumn.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVColumn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVColumn.Font = new System.Drawing.Font("Tahoma", 10F);
            this.LVColumn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVColumn.FullRowSelect = true;
            this.LVColumn.GridLines = true;
            this.LVColumn.HideSelection = false;
            this.LVColumn.Location = new System.Drawing.Point(0, 30);
            this.LVColumn.Name = "LVColumn";
            this.LVColumn.Size = new System.Drawing.Size(898, 459);
            this.LVColumn.TabIndex = 0;
            this.LVColumn.UseCompatibleStateImageBehavior = false;
            this.LVColumn.View = System.Windows.Forms.View.Details;
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(898, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 201;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "3. Cột dữ liệu";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.btnAutoCode);
            this.panel1.Controls.Add(this.txt_Path);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 561);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1352, 249);
            this.panel1.TabIndex = 210;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txt_Aspx_Edit);
            this.groupBox1.Controls.Add(this.txt_Aspx_List);
            this.groupBox1.Controls.Add(this.txt_InfoClass);
            this.groupBox1.Controls.Add(this.txt_DataClass);
            this.groupBox1.Controls.Add(this.txt_ModelClass);
            this.groupBox1.Controls.Add(this.txt_Using);
            this.groupBox1.Controls.Add(this.txt_NameSpace);
            this.groupBox1.Controls.Add(this.cbo_Connect);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.chkData);
            this.groupBox1.Controls.Add(this.chk_Edit);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.chk_List);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.chkInfo);
            this.groupBox1.Controls.Add(this.chkModel);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox1.ForeColor = System.Drawing.Color.Navy;
            this.groupBox1.Location = new System.Drawing.Point(15, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1316, 195);
            this.groupBox1.TabIndex = 117;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tùy chọn tạo code";
            // 
            // txt_Aspx_Edit
            // 
            this.txt_Aspx_Edit.Location = new System.Drawing.Point(190, 153);
            this.txt_Aspx_Edit.Multiline = true;
            this.txt_Aspx_Edit.Name = "txt_Aspx_Edit";
            this.txt_Aspx_Edit.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Aspx_Edit.Size = new System.Drawing.Size(320, 26);
            this.txt_Aspx_Edit.StateCommon.Border.ColorAngle = 1F;
            this.txt_Aspx_Edit.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Aspx_Edit.StateCommon.Border.Rounding = 4;
            this.txt_Aspx_Edit.StateCommon.Border.Width = 1;
            this.txt_Aspx_Edit.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Aspx_Edit.TabIndex = 4;
            this.txt_Aspx_Edit.Visible = false;
            // 
            // txt_Aspx_List
            // 
            this.txt_Aspx_List.Location = new System.Drawing.Point(190, 121);
            this.txt_Aspx_List.Multiline = true;
            this.txt_Aspx_List.Name = "txt_Aspx_List";
            this.txt_Aspx_List.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Aspx_List.Size = new System.Drawing.Size(320, 26);
            this.txt_Aspx_List.StateCommon.Border.ColorAngle = 1F;
            this.txt_Aspx_List.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Aspx_List.StateCommon.Border.Rounding = 4;
            this.txt_Aspx_List.StateCommon.Border.Width = 1;
            this.txt_Aspx_List.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Aspx_List.TabIndex = 3;
            this.txt_Aspx_List.Visible = false;
            // 
            // txt_InfoClass
            // 
            this.txt_InfoClass.Location = new System.Drawing.Point(190, 89);
            this.txt_InfoClass.Multiline = true;
            this.txt_InfoClass.Name = "txt_InfoClass";
            this.txt_InfoClass.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_InfoClass.Size = new System.Drawing.Size(320, 26);
            this.txt_InfoClass.StateCommon.Border.ColorAngle = 1F;
            this.txt_InfoClass.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_InfoClass.StateCommon.Border.Rounding = 4;
            this.txt_InfoClass.StateCommon.Border.Width = 1;
            this.txt_InfoClass.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_InfoClass.TabIndex = 2;
            // 
            // txt_DataClass
            // 
            this.txt_DataClass.Location = new System.Drawing.Point(190, 55);
            this.txt_DataClass.Multiline = true;
            this.txt_DataClass.Name = "txt_DataClass";
            this.txt_DataClass.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_DataClass.Size = new System.Drawing.Size(320, 26);
            this.txt_DataClass.StateCommon.Border.ColorAngle = 1F;
            this.txt_DataClass.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_DataClass.StateCommon.Border.Rounding = 4;
            this.txt_DataClass.StateCommon.Border.Width = 1;
            this.txt_DataClass.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_DataClass.TabIndex = 1;
            // 
            // txt_ModelClass
            // 
            this.txt_ModelClass.Location = new System.Drawing.Point(190, 23);
            this.txt_ModelClass.Multiline = true;
            this.txt_ModelClass.Name = "txt_ModelClass";
            this.txt_ModelClass.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_ModelClass.Size = new System.Drawing.Size(320, 26);
            this.txt_ModelClass.StateCommon.Border.ColorAngle = 1F;
            this.txt_ModelClass.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_ModelClass.StateCommon.Border.Rounding = 4;
            this.txt_ModelClass.StateCommon.Border.Width = 1;
            this.txt_ModelClass.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ModelClass.TabIndex = 0;
            // 
            // txt_Using
            // 
            this.txt_Using.Location = new System.Drawing.Point(990, 94);
            this.txt_Using.Multiline = true;
            this.txt_Using.Name = "txt_Using";
            this.txt_Using.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Using.Size = new System.Drawing.Size(320, 26);
            this.txt_Using.StateCommon.Border.ColorAngle = 1F;
            this.txt_Using.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Using.StateCommon.Border.Rounding = 4;
            this.txt_Using.StateCommon.Border.Width = 1;
            this.txt_Using.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Using.TabIndex = 7;
            // 
            // txt_NameSpace
            // 
            this.txt_NameSpace.Location = new System.Drawing.Point(990, 60);
            this.txt_NameSpace.Multiline = true;
            this.txt_NameSpace.Name = "txt_NameSpace";
            this.txt_NameSpace.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_NameSpace.Size = new System.Drawing.Size(320, 26);
            this.txt_NameSpace.StateCommon.Border.ColorAngle = 1F;
            this.txt_NameSpace.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_NameSpace.StateCommon.Border.Rounding = 4;
            this.txt_NameSpace.StateCommon.Border.Width = 1;
            this.txt_NameSpace.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_NameSpace.TabIndex = 6;
            this.txt_NameSpace.Text = "TN_Library";
            // 
            // cbo_Connect
            // 
            this.cbo_Connect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_Connect.DropDownWidth = 268;
            this.cbo_Connect.Items.AddRange(new object[] {
            "TN_Helper.ConnectionString",
            "ConfigurationManager.ConnectionStrings[\"ConnectionString\"].ConnectionString",
            "ConnectDataBase.ConnectionString"});
            this.cbo_Connect.Location = new System.Drawing.Point(990, 28);
            this.cbo_Connect.Name = "cbo_Connect";
            this.cbo_Connect.Size = new System.Drawing.Size(320, 29);
            this.cbo_Connect.TabIndex = 5;
            this.cbo_Connect.Text = "TN_Helper.ConnectionString";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(819, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(148, 22);
            this.label8.TabIndex = 116;
            this.label8.Text = "Connection string";
            // 
            // chkData
            // 
            this.chkData.AutoSize = true;
            this.chkData.Checked = true;
            this.chkData.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkData.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkData.ForeColor = System.Drawing.Color.Navy;
            this.chkData.Location = new System.Drawing.Point(17, 60);
            this.chkData.Name = "chkData";
            this.chkData.Size = new System.Drawing.Size(134, 25);
            this.chkData.TabIndex = 28;
            this.chkData.Text = "_Data Class";
            this.chkData.UseVisualStyleBackColor = true;
            // 
            // chk_Edit
            // 
            this.chk_Edit.AutoSize = true;
            this.chk_Edit.Checked = true;
            this.chk_Edit.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_Edit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_Edit.ForeColor = System.Drawing.Color.Navy;
            this.chk_Edit.Location = new System.Drawing.Point(17, 158);
            this.chk_Edit.Name = "chk_Edit";
            this.chk_Edit.Size = new System.Drawing.Size(131, 25);
            this.chk_Edit.TabIndex = 27;
            this.chk_Edit.Text = "Aspx Popup";
            this.chk_Edit.UseVisualStyleBackColor = true;
            this.chk_Edit.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(870, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 22);
            this.label6.TabIndex = 116;
            this.label6.Text = "Using ...";
            // 
            // chk_List
            // 
            this.chk_List.AutoSize = true;
            this.chk_List.Checked = true;
            this.chk_List.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_List.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_List.ForeColor = System.Drawing.Color.Navy;
            this.chk_List.Location = new System.Drawing.Point(17, 126);
            this.chk_List.Name = "chk_List";
            this.chk_List.Size = new System.Drawing.Size(107, 25);
            this.chk_List.TabIndex = 27;
            this.chk_List.Text = "Aspx List";
            this.chk_List.UseVisualStyleBackColor = true;
            this.chk_List.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(847, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 22);
            this.label4.TabIndex = 116;
            this.label4.Text = "Name Space";
            // 
            // chkInfo
            // 
            this.chkInfo.AutoSize = true;
            this.chkInfo.Checked = true;
            this.chkInfo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkInfo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInfo.ForeColor = System.Drawing.Color.Navy;
            this.chkInfo.Location = new System.Drawing.Point(17, 94);
            this.chkInfo.Name = "chkInfo";
            this.chkInfo.Size = new System.Drawing.Size(125, 25);
            this.chkInfo.TabIndex = 27;
            this.chkInfo.Text = "_Info Class";
            this.chkInfo.UseVisualStyleBackColor = true;
            // 
            // chkModel
            // 
            this.chkModel.AutoSize = true;
            this.chkModel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkModel.ForeColor = System.Drawing.Color.Navy;
            this.chkModel.Location = new System.Drawing.Point(17, 27);
            this.chkModel.Name = "chkModel";
            this.chkModel.Size = new System.Drawing.Size(145, 25);
            this.chkModel.TabIndex = 26;
            this.chkModel.Text = "_Model Class";
            this.chkModel.UseVisualStyleBackColor = true;
            // 
            // btnAutoCode
            // 
            this.btnAutoCode.Location = new System.Drawing.Point(1173, 212);
            this.btnAutoCode.Name = "btnAutoCode";
            this.btnAutoCode.Size = new System.Drawing.Size(158, 25);
            this.btnAutoCode.TabIndex = 1;
            this.btnAutoCode.Values.Text = "Tạo code";
            // 
            // txt_Path
            // 
            this.txt_Path.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnOpenFolder});
            this.txt_Path.Location = new System.Drawing.Point(136, 212);
            this.txt_Path.Multiline = true;
            this.txt_Path.Name = "txt_Path";
            this.txt_Path.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Path.Size = new System.Drawing.Size(616, 26);
            this.txt_Path.StateCommon.Border.ColorAngle = 1F;
            this.txt_Path.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Path.StateCommon.Border.Rounding = 4;
            this.txt_Path.StateCommon.Border.Width = 1;
            this.txt_Path.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Path.TabIndex = 0;
            // 
            // btnOpenFolder
            // 
            this.btnOpenFolder.Edge = ComponentFactory.Krypton.Toolkit.PaletteRelativeEdgeAlign.Far;
            this.btnOpenFolder.Style = ComponentFactory.Krypton.Toolkit.PaletteButtonStyle.ButtonSpec;
            this.btnOpenFolder.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.WorkspaceMaximize;
            this.btnOpenFolder.UniqueName = "20643A79AAC1497DA4A9146891E9A4AD";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(27, 219);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 22);
            this.label5.TabIndex = 116;
            this.label5.Text = "Thư mục tập tin";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 44);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1366, 850);
            this.tabControl1.TabIndex = 211;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 33);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1358, 813);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "AutoCode";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer2);
            this.tabPage2.Location = new System.Drawing.Point(4, 33);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1358, 813);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "AutoString";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.txt_Before);
            this.splitContainer2.Panel1.Controls.Add(this.kryptonHeader4);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.txt_After);
            this.splitContainer2.Panel2.Controls.Add(this.kryptonHeader5);
            this.splitContainer2.Size = new System.Drawing.Size(1352, 807);
            this.splitContainer2.SplitterDistance = 665;
            this.splitContainer2.TabIndex = 0;
            // 
            // txt_Before
            // 
            this.txt_Before.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_Before.Location = new System.Drawing.Point(0, 30);
            this.txt_Before.Multiline = true;
            this.txt_Before.Name = "txt_Before";
            this.txt_Before.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_Before.Size = new System.Drawing.Size(665, 777);
            this.txt_Before.TabIndex = 0;
            // 
            // kryptonHeader4
            // 
            this.kryptonHeader4.AutoSize = false;
            this.kryptonHeader4.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btn_AutoString});
            this.kryptonHeader4.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader4.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader4.Name = "kryptonHeader4";
            this.kryptonHeader4.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader4.Size = new System.Drawing.Size(665, 30);
            this.kryptonHeader4.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader4.TabIndex = 201;
            this.kryptonHeader4.Values.Description = "";
            this.kryptonHeader4.Values.Heading = "1";
            // 
            // btn_AutoString
            // 
            this.btn_AutoString.Text = "Chuyển StringBuilder";
            this.btn_AutoString.UniqueName = "025F6057A37F4531CBA7BAAB36783125";
            // 
            // txt_After
            // 
            this.txt_After.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_After.Location = new System.Drawing.Point(0, 30);
            this.txt_After.Multiline = true;
            this.txt_After.Name = "txt_After";
            this.txt_After.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_After.Size = new System.Drawing.Size(683, 777);
            this.txt_After.TabIndex = 1;
            // 
            // kryptonHeader5
            // 
            this.kryptonHeader5.AutoSize = false;
            this.kryptonHeader5.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btn_Clear});
            this.kryptonHeader5.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader5.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader5.Name = "kryptonHeader5";
            this.kryptonHeader5.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader5.Size = new System.Drawing.Size(683, 30);
            this.kryptonHeader5.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader5.TabIndex = 201;
            this.kryptonHeader5.Values.Description = "";
            this.kryptonHeader5.Values.Heading = "2";
            // 
            // btn_Clear
            // 
            this.btn_Clear.Text = "Làm mới";
            this.btn_Clear.UniqueName = "F4EA78D7C35F415DBA9CEB16CCA0CB64";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1366, 894);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtTitle);
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.ForeColor = System.Drawing.Color.Navy;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Server)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Password)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_UserName)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.pnTable.ResumeLayout(false);
            this.pnDatabase.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Connect)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        
        private System.Windows.Forms.Label label1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader txtTitle;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_Server;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel pnTable;
        private System.Windows.Forms.ListView LVTable;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private System.Windows.Forms.Panel pnDatabase;
        private System.Windows.Forms.ListView LVDatabase;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader3;
        private System.Windows.Forms.ListView LVColumn;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.Panel panel1;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_InfoClass;
        private System.Windows.Forms.GroupBox groupBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_DataClass;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_ModelClass;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_NameSpace;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_Connect;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkData;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkInfo;
        private System.Windows.Forms.CheckBox chkModel;
        private System.Windows.Forms.Label label5;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Path;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnOpenFolder;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnAutoCode;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnConnect;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Using;
        private System.Windows.Forms.Label label6;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Aspx_Edit;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Aspx_List;
        private System.Windows.Forms.CheckBox chk_Edit;
        private System.Windows.Forms.CheckBox chk_List;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_Password;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_UserName;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TextBox txt_Before;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader4;
        private System.Windows.Forms.TextBox txt_After;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader5;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btn_AutoString;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btn_Clear;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
    }
}